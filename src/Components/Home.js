import React from "react";
import "./styles/Home.css";

function Home() {
  return (
    <div className="home">
      
      <ul className="userDetails">
        <li>
          <h1>Hi, there</h1>
        </li>
        <li>
          <h2>My name is Alen George</h2>
        </li>
        <li>I am a Software Engineer</li>
      </ul>
    </div>
  );
}

export default Home;
