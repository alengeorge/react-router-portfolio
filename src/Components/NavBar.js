import React from "react";
import { Link } from "react-router-dom";
import styles from "./styles/NavBar.module.css";

function NavBar() {
  return (
    <nav>
      <ul className={styles.navBar}>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
        <li>
          <Link to="/contact">Contact</Link>
        </li>
      </ul>
    </nav>
  );
}

export default NavBar;
