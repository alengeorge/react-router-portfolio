import React from "react";
import "./styles/About.css";

function About() {
  return (
    <div className="about">
      {/* <h2>About Me</h2> */}
      <div className="dp">
        <img src={"/images/picdp.jpeg"} alt="dp of user" />
      </div>
      <ul>
        <li>Hometown - Kottayam,Kerala </li>
        <li> Education - B.Tech in Computer Science and Engineering.</li>
        <li>College - College of Engineering Chengannur.</li>
        <li>Hobbies - Watching movies, Cycling & Swimming.</li>
        <li>Technologies used - Python for Data Science, HTML, CSS & C++.</li>
      </ul>
    </div>
  );
}

export default About;
