import React, { Component } from "react";
import './styles/contact.css'
class Contact extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         name:"",
         email:"",
         reason:"",
         submit:false,
         logMessage:""
      }
    
    }

    onsubmit = (event)=>{
      event.preventDefault()
      if(this.state.name&&this.state.email){
      this.setState({
            submit:true,
            logMessage:"Submitted Successfully"
          })
        }
      else{
        this.setState({
        
          logMessage:"Submission failed"
        })

      }
      }

    HandleInput = (event) =>{
      // console.log(event.target.id);
      switch(event.target.id){
      case 'name': 
      this.setState({
        name:event.target.value
      });
      break;

      case 'email':
        this.setState({
          email:event.target.value
        });
        break;

      case 'reason':
        this.setState({
          reason:event.target.value
        });
        break;

      default:
        this.setState({
          // submit:false,
          logMessage:"Submission failed"

        });

    }

    }
    
 
  render() {
    return (
      <div className="map">
        <iframe 
          title={"mylocation"}
          src={"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.6022063509618!2d77.60984591479982!3d12.933269290881034!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14526d47175f%3A0x8a51177fba6ca896!2s91springboard%207th%20Block%2C%20Koramangala!5e0!3m2!1sen!2sin!4v1651319395891!5m2!1sen!2sin"}
          
       

          // allowFullScreen={""}
          // loading={"lazy"}
          // referrerPolicy={"no-referrer-when-downgrade"}
        ></iframe>
        <form onSubmit={this.onsubmit} className="formData">
          <label htmlFor="name">Full Name</label>
          <input id="name" type="text" value={this.state.name} onChange={this.HandleInput}/>
          <label htmlFor="email">Email</label>
          <input id="email" type="text" value={this.state.email} onChange={this.HandleInput}/>
          <label htmlFor="reason">Reason for Contact</label>
          <input id="reason" type="text" value={this.state.reason} onChange={this.HandleInput}/>
          <input type="submit" value="submit"></input>
          {<p className={this.state.submit?"valid":"invalid"}>{this.state.logMessage}</p>}

        </form>

      </div>
    );
  }
}

export default Contact;
